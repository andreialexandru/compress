module Compress(compress, decompress, main) where

import System.Environment ( getArgs )
import qualified Data.ByteString as ByteString


compress :: ByteString.ByteString -> ByteString.ByteString
compress buff = buff


decompress :: ByteString.ByteString -> ByteString.ByteString
decompress buf = buf


compress_file :: FilePath -> IO ()
compress_file f_in = do
    contents <- ByteString.readFile f_in

    let compressed = compress contents
    ByteString.writeFile (f_in ++ ".compr") compressed

    let decompressed = decompress compressed
    ByteString.writeFile (f_in ++ ".decompr") decompressed

main :: IO ()
main = do
    f_in:[] <- getArgs
    putStrLn "Input file: "
    putStrLn f_in

    compress_file f_in