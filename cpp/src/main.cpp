#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <optional>
#include <unordered_map>
#include <vector>
#include <cassert>


class Compressor {
public:
    // Using a vector for large files is not a great idea. Maybe deque?
    using byte_t = std::byte;
    using buffer_t = std::vector<byte_t>;

    static buffer_t deflate(const buffer_t& input) {
        assert(!input.empty() && "Input cannot be empty");

        // Create a map of (unique symbol, index), eg: {{'A', 0}, {'T', 1}, {'C', 2}}
        int symbol_index = 0;
        std::unordered_map<byte_t, byte_t> symbol_mapping;
        for(const auto& symbol : input) {
            auto [_, inserted] = symbol_mapping.try_emplace(symbol, byte_t(symbol_index));
            if (inserted) {
                symbol_index++;
            }
        }

        int unq_sym_count = symbol_mapping.size();
        int bits_per_symbol = std::ceil(std::log2(unq_sym_count));
        if (bits_per_symbol == 0) {
            // If there's only one type of symbol, we need a bit
            bits_per_symbol = 1;
        }

        // Get the compressed content size by multiplying the number of bytes in the input
        // with the compression factor of one byte.
        int compressed_size = std::ceil(input.size() * (bits_per_symbol / 8.0));

        buffer_t compressed;
        compressed.reserve(compressed_size);

        // COMPRESSION
        // Add the symbols one by one to a buffer, then take the last byte and save it to the `compressed` buffer
        unsigned int buff = 0;
        unsigned int bits_in_buff = 0;
        for (auto it = std::rbegin(input); it != std::rend(input); ++it) {
            unsigned int symbol = static_cast<unsigned int>(symbol_mapping[*it]);
            symbol <<= bits_in_buff;
            buff |= symbol;

            // If we shifted passed a byte's size, go to the next byte
            bits_in_buff += bits_per_symbol;
            if (bits_in_buff >= 8) {
                compressed.push_back(byte_t(buff & 0xff));
                bits_in_buff -= 8;
                buff >>= 8;
            }
        }

        // If there are still bits in buff, write them in a new byte, with 0 padding
        if (bits_in_buff != 0) {
            compressed.push_back(byte_t(buff & 0xff));
        }

        // 1 byte to hold the number of unique symbols - 1.
        // unq_sym_count bytes, one for every unique symbol, to be able to map them back when inflating
        // 1 byte for number of padding bits in the first content byte, since the compressed content might not divide evenly to 8
        int header_size = 1 + unq_sym_count + 1;

        buffer_t output;
        output.reserve(header_size + compressed.size());

        // WRITE HEADER
        output.resize(header_size);

        // 1 <= unq_sym_count <= 256, so we need to substract 1 to fit in a byte
        output[0] = byte_t(unq_sym_count - 1);
        for (const auto& kv : symbol_mapping) {
            auto sym_i = static_cast<size_t>(kv.second);
            assert(sym_i >= 0 && sym_i < output.size() && "Invalid symbol index");
            output[sym_i + 1] = kv.first;
        }
        int padding_size = (8 - bits_in_buff) % 8;
        output.back() = byte_t(padding_size);

        // WRITE CONTENT
        // The compression algorithm produces the output reversed, so it needs to be reversed back
        std::reverse_copy(compressed.begin(), compressed.end(), std::back_inserter(output));

        return output;
    }

    static buffer_t inflate(const buffer_t& input) {
        assert(!input.empty());

        // HEADER
        // Read unique symbols count
        int unq_sym_count = static_cast<int>(input.front()) + 1; // Add back the 1 substracted to fit in a byte.
        int bits_per_symbol = std::ceil(std::log2(unq_sym_count));
        if (bits_per_symbol == 0) {
            bits_per_symbol = 1;
        }

        // Read the symbols mapping, eg 00 -> A, 01 -> T, 10 -> C
        buffer_t symbol_mapping;
        symbol_mapping.reserve(unq_sym_count);
        std::copy(input.begin() + 1, input.begin() + unq_sym_count + 1, std::back_inserter(symbol_mapping));

        // Read the first byte's padding
        int padding_size = static_cast<int>(input[unq_sym_count + 1]);


        auto content_begin = input.begin() + unq_sym_count + 2;
        int content_size = std::distance(content_begin, input.end());
        int decompressed_size = std::ceil(content_size * (8.0 / bits_per_symbol));

        buffer_t output;
        output.reserve(decompressed_size);

        // DECOMPRESSION

        // The algorithm works from right to left, on two bytes at a time,
        // so the map is shifted to the left of those two bytes.
        // For example, for bits_per_symbol == 3,
        // mask = 000...0  11100000  00000000
        unsigned int mask = ~(0xff >> bits_per_symbol);
        mask &= 0xff;
        mask <<= 8;

        unsigned int buff = 0;
        int shift_r = padding_size; // Skip the padding
        for (auto it = content_begin; it != std::end(input); ++it) {
            // Read two bytes at a time, since there are symbols split between bytes
            buff = static_cast<unsigned int>(*it);
            buff = buff << 8;
            if (std::next(it) != std::end(input)){
                buff |= static_cast<unsigned int>(*std::next(it));
            }

            // Iterate the symbol indices in the buffer and write them to individual bytes
            for(;shift_r < 8; shift_r += bits_per_symbol) {
                unsigned int symbol_index = buff & (mask >> shift_r);
                symbol_index >>= 16 - (shift_r + bits_per_symbol);

                output.push_back(symbol_mapping[symbol_index]);
            }

            buff <<= 8;
            buff &= 0xff;
            shift_r -= 8;
        }

        return output;
    }
};

std::optional<Compressor::buffer_t> read_from_file(const std::string& file_name) {
    std::ifstream istream(file_name, std::ios::in | std::ios::binary | std::ios::ate);
    if (!istream.is_open()) {
        return std::nullopt;
    }

    auto fsize = istream.tellg();
    Compressor::buffer_t in_data(fsize);
    istream.seekg(0);

    istream.read(reinterpret_cast<char*>(in_data.data()), in_data.size());

    // Moves buffer inside optional
    return in_data;
}

bool write_to_file(const std::string& file_name, const Compressor::buffer_t& output) {
    std::ofstream ostream(file_name, std::ios::out | std::ios::binary);
    if (!ostream.is_open()) {
        return false;
    }

    ostream.write(reinterpret_cast<const char*>(output.data()), output.size());
    return true;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cerr << "Invalid number of arguments.\n";
        std::cout << "Usage: compress <input_file> <output_file>\n";

        return -1;
    }

    std::string in_file_name = argv[1];
    std::string out_file_name = argv[2];

    std::cout << "Reading from file \"" << in_file_name << "\" and writing to file \"" << out_file_name << "\"\n";

    std::optional<Compressor::buffer_t> input_opt = read_from_file(in_file_name); // std::optional < std::expected
    if (!input_opt) {
        std::cerr << "Failed to read from file " << in_file_name << '\n';

        return -1;
    }

    // Move buffer out of optional
    Compressor::buffer_t input_data = std::move(*input_opt);
    Compressor::buffer_t compressed = Compressor::deflate(input_data);

    std::cout << "Writing compressed to file \"" << out_file_name <<"\"\n";
    if (!write_to_file(out_file_name, compressed)) {
        std::cerr << "Failed to write to file " << out_file_name << '\n';

        return -1;
    }


    Compressor::buffer_t decompressed = Compressor::inflate(compressed);

    std::string dec_file_name = in_file_name + ".dec";
    std::cout << "Writing decompressed to file \"" << dec_file_name <<"\"\n";
    if (!write_to_file(dec_file_name, decompressed)) {
        std::cerr << "Failed to write to file " << dec_file_name << '\n';

        return -1;
    }

    std::cout << "Success!\n";
    return 0;
}
