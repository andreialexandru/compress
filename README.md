Task
---
For a data processing task, we have an input file where each byte represents a symbol in an input sequence. We wish to store this sequence in RAM and be able to access symbol at a given index in constant time​. However, the large length of the sequence is problematic due to memory constraints. The symbols in the sequence generally in a non-repeating order, and the probability distribution of the symbols is close to uniform. In practice, the number of unique symbols in the sequence is small (often ≤ 16) but may take arbitrary values. For example, when rendered using ASCII, an input file might contain:

`RYKARRKAAGCWATYWKMTCTWRAGSRMGCTKGTKCACMYACYTTMG`

Tasks:
   1. An application which takes an input file and produces an output file which contains a compact representation of the input file.
   2. An application which takes the compact representation and regenerates the original file.

Instructions
---
Run ```make``` in the project root to build, then ```make test``` to run the tests.

Prerequisits: Linux-like env. (Linux/Windows Subsystem For Linux/cygwin), clang with C++17 support.

Description
---
The compression algorithm looks for the alphabet size, and chooses the maximum number of bits needed to store all the symbols.
It encodes every symbol as a number, and saves it to a buffer together with a header describing the symbol to index mapping.
```
   | SymobolCount | Symbols | PaddingAmmount | Content |
   |     1 byte   | #Symbols|    1 byte      |
   |         <----  HEADER  ---->            |
```
##### Example
Original File: `RYKARRKAA` =>

Alphabet: `RYKA`
The alphabet has 4 elements) => 2 bit index needed:
```
 R  Y  K  A
00 01 10 11

| 4 | R  Y  K  A | 6 | 00 00 00 00   01 10 11 00   00 10 11 11 |
                      |padding|  R    Y  K  A  R    R  K  A  A
```

TODO
---
* Add proper tests. Currently it just compresses and uncompresses and compares the output with the original.
* Split code in lib / cli.
* Better error checking and handling. There's hardly any atm.
* Cmake instead of an adhoc makefile.
* ...
